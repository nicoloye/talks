Présentations
=============

Ces présentations sont basées sur [AccesSlide](https://github.com/access42/AccesSlide), un framework pour réaliser des présentations accessibles en HTML5-CSS3-JS.

En particulier, elles implémentent le fork de [ffoodd](https://github.com/ffoodd/Talks).